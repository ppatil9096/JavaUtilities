package com.pravin.statement;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

public class CallableStatementDemo {

    static {
	try {
	    Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
	    e.printStackTrace();
	}
    }

    public static void main(String[] args) throws SQLException {

	Connection conn = DriverManager.getConnection(
		"jdbc:mysql://localhost:3306/pravintest?autoReconnect=true&useSSL=false", "root", "root");
	// callableWithoutParam(conn);
	// callableWithInParam(conn);
	callableWithInOutParam(conn);
	conn.close();
    }

    public static void callableWithoutParam(Connection conn) throws SQLException {
	CallableStatement callableStatement = conn.prepareCall("call pp_procedure");
	callableStatement.execute();
	callableStatement.close();
    }

    public static void callableWithInParam(Connection conn) throws SQLException {
	CallableStatement callableStatement = conn.prepareCall("{call pp_procedureIn('Akshay',1)}");
	callableStatement.execute();
	callableStatement.close();
    }

    public static void callableWithInOutParam(Connection conn) throws SQLException {
	CallableStatement callableStatement = conn.prepareCall("{call pp_procedureInOut(?,?)}");
	callableStatement.setInt(1, 1);
	callableStatement.registerOutParameter(2, Types.INTEGER);
	callableStatement.execute();
	int age = callableStatement.getInt(2);
	System.out.println("Age :: " + age);
	callableStatement.close();
    }
}
