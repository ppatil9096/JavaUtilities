create database pravintest;

CREATE TABLE pravintest.student (
  id int(11) NOT NULL,
  name varchar(20) DEFAULT NULL,
  department varchar(20) DEFAULT NULL,
  age int(11) DEFAULT NULL,
  PRIMARY KEY (id)
);

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `pp_procedure`()
BEGIN
insert into pravintest.student(id,name,department,age) values(1,'pravin','cse',24);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `pp_procedureIn`(in PARAMETER1 VARCHAR(50), PARAMETER2 int)
BEGIN
UPDATE pravintest.student SET name = PARAMETER1 WHERE ID = PARAMETER2;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `pp_procedureInOut`(in PARAMETER1 int, out PARAMETER2 int)
BEGIN
SELECT age INTO PARAMETER2 FROM pravintest.student WHERE ID = PARAMETER1;
END$$
DELIMITER ;
