package com.pravin.sorting;

public class BubbleSort {
    public static void main(String[] args) {
	final int[] number = { 4, 89, 73, 2, 5, 17 };
	bubbleSort(number);
    }

    private static void bubbleSort(int[] number) {
	boolean isSwitched = false;
	do {
	    System.out.println(";");
	    int length = number.length;
	    for (int i = 0; i < length - 1; i++) {
		if (number[i + 1] < number[i]) {
		    int tmp = number[i + 1];
		    number[i + 1] = number[i];
		    number[i] = tmp;
		    isSwitched = true;
		}
	    }
	} while (isSwitched);
	for (int i : number) {
	    System.out.println("\t" + i);
	}
    }
}
