package com.pravin.compare;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CompareDemo {
    public static void main(String[] args) {
	/*
	 * // demo 1 final int[] numbers = { -1, -5, -8, 1, 4, 8, 2 }; final String[]
	 * strings = { "z", "x", "y", "abc", "zzz", "zazzy" }; for (int i : numbers) {
	 * System.out.print(i + "\t"); } Arrays.sort(numbers);
	 * System.out.println("\nAfter ::"); for (int i : numbers) { System.out.print(i
	 * + "\t"); } System.out.println(
	 * "\n==========================================================="); for (String
	 * string : strings) { System.out.print(string + "\t"); }
	 * System.out.println("\nAfter ::"); Arrays.sort(strings); for (String string :
	 * strings) { System.out.print(string + "\t"); }
	 */
	/*
	 * // demo 2 final List<NotComparable> list = new ArrayList<>(); for (int i = 0;
	 * i < 10; i++) { list.add(new NotComparable(i)); }
	 * Arrays.sort(list.toArray());// class cast exception
	 */
	// demo 3

	final List<Integer> numbersList = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
	System.out.println("Asc order ::");
	Collections.sort(numbersList, new NumericalOrder());
	numbersList.forEach(s -> System.out.print(s + "\t"));

	System.out.println("\nDesc order :: ");
	Collections.sort(numbersList, new ReverseNumericalOrder());
	numbersList.forEach(s -> System.out.print(s + "\t"));
    }
}
