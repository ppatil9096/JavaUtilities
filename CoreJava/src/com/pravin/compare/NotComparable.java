package com.pravin.compare;

public class NotComparable {
    private int i;

    public NotComparable(final int i) {
	this.setI(i);
    }

    public int getI() {
	return i;
    }

    public void setI(int i) {
	this.i = i;
    }
}
