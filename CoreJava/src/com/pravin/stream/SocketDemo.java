package com.pravin.stream;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.Socket;

public class SocketDemo {
    public static void main(String[] args) {
	try {
	    Socket s = new Socket("www.google.com", 80);
	    InputStream in = s.getInputStream();
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    int reads = in.read();

	    while (reads != -1) {
		baos.write(reads);
		reads = in.read();
	    }

	    byte buf[] = baos.toByteArray();
	    for (byte b : buf) {
		System.out.println(b);
	    }
	    // OutputStream out = s.getOutputStream();
	    // String str = "java2s.com\n";
	    // str.getBytes();
	    // out.write(buf);
	    // while ((c = in.read()) != -1) {
	    // System.out.print((char) c);
	    // }
	    s.close();
	} catch (Exception e) {
	    System.err.println("Exception:  " + e);
	}
    }
}
