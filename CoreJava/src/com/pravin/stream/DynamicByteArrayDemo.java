package com.pravin.stream;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DynamicByteArrayDemo {
    public static void main(String[] args) {
	// Converting InputStream to Byte arrray using Java code
	int length = 0;
	try (FileInputStream fileInputStream = new FileInputStream("/home/pp/Desktop/a.txt")) {
//	    length = toByteArrayUsingJavaRead(fileInputStream).length;
	    System.out.println("Length of Byte array :: " + length);
	    length = toByteArrayUsingJavaParamRead(fileInputStream).length;
	    System.out.println("Length of Byte array :: " + length);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /*
     * Read bytes from inputStream and writes to OutputStream, later converts
     * OutputStream to byte array in Java.
     */
    public static byte[] toByteArrayUsingJavaRead(InputStream is) throws IOException {
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	int reads = is.read();

	while (reads != -1) {
	    System.out.println(reads);
	    baos.write(reads);
	    reads = is.read();
	}

	return baos.toByteArray();

    }

    public static byte[] toByteArrayUsingJavaParamRead(InputStream is) throws IOException {
	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	int nRead;
	byte[] data = new byte[16384];
	while ((nRead = is.read(data, 0, data.length)) != -1) {
	    System.out.println(nRead);
	    buffer.write(data, 0, nRead);
	}

	buffer.flush();
	return buffer.toByteArray();

    }

}
