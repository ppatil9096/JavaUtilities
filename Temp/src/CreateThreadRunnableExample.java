public class CreateThreadRunnableExample implements Runnable {

	public void run() {

		for (int i = 0; i < 10; i++) {
			System.out.println("Child Thread : " + i);

			try {
				Thread.sleep(100);
			} catch (InterruptedException ie) {
				System.out.println("Child thread interrupted! " + ie);
			}
		}

		System.out.println("Child thread finished!");
	}

	public static void main(String[] args) {

		/*
		 * To create new thread, use Thread(Runnable thread, String threadName)
		 * constructor.
		 *
		 */

		Thread t = new Thread(new CreateThreadRunnableExample(), "My Thread");
		Thread currentThread = Thread.currentThread();
		System.out.println(currentThread);
		currentThread.setName("Set Thread Name Example");
		System.out.println(currentThread);
		/*
		 * To start a particular thread, use void start() method of Thread class.
		 *
		 * Please note that, after creation of a thread it will not start running until
		 * we call start method.
		 */
		t.start();

		for (int i = 0; i < 10; i++) {

			System.out.println("Main thread : " + i);

			try {
				Thread.sleep(100);
			} catch (InterruptedException ie) {
				System.out.println("Child thread interrupted! " + ie);
			}
		}
		System.out.println("Main thread finished!");
	}
}