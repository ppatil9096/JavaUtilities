import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


public class UpdateStatus {
        private Connection con = null;
        private ResultSet rs = null;
        private Statement stmt = null;

        public UpdateStatus(String dbServer, String dbLogin, String dbPassword) {
            ConnectionPoolObject vCon = new ConnectionPoolObject();
            try {
                con = vCon.getConnection(dbServer, dbLogin, dbPassword);
                System.out.println("Connection : " + con);
            } catch (Exception se) {
                System.out.println(se.toString());
            } finally {
                try {
                    if (vCon != null) {
                        vCon.closeConnection(dbServer, dbLogin);
                    }
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        }

        public void close() {
            try {
                if (con != null) {
                    System.out.println("Closing the connection here");
                    if (rs != null) {
                        rs.close();
                    }
                    con.close();
                }
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        public boolean updateStatusforBatchRecords(String fileTrackingNo, String processorName) {
            boolean status = false;
            StringBuffer strQuery = null;
            try {
                System.out.println(" ENTRY INTO updateStatusforBatchRecords() ");

                stmt = con.createStatement();

                strQuery = new StringBuffer();
                strQuery.append("UPDATE BATCH_SETTLEMENT_MASTER SET BS_TRANSACTION_SETTLE_STATUS = '3',BS_FILE_TRACKING_NUMBER = '" + fileTrackingNo + "' ");
                strQuery.append("WHERE BS_PROCESSOR_NAME ='" + processorName + "' AND BS_TRANSACTION_SETTLE_STATUS = '0'");

                System.out.println(strQuery.toString());
                int i = stmt.executeUpdate(strQuery.toString());
                if (i > 0) {
                    status = true;
                    System.out.println("UPDATED SUCCEEFULLY " + i);
                }

            } catch (SQLException ex) {
                System.out.println("SQLException: " + ex.getMessage());
            } catch (Exception e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println(" Exit Off updateStatusforBatchRecords() ");
            return status;
        }

        public void updateStauts(List<String> approvedTicketList, List<String> offLineApprovedTicketList) {

            final int batchSize = 900;
            int count = 0;
            // StringBuilder ticketList = new StringBuilder();
            StringBuilder tUpdateQuery = new StringBuilder("UPDATE RESA_TENDER_DETAILS SET TD_SETTLE_STATUS = '1' WHERE TD_PROCESSOR_ID='603' AND TD_SETTLE_STATUS='0' AND TD_AURUSPAY_TICKET_NUMBER IN (?) ");
            System.out.println("Query : " + tUpdateQuery.toString());
            System.out.println("Online Ticket's to Update : " + approvedTicketList.size());

            try {
                PreparedStatement ps = con.prepareStatement(tUpdateQuery.toString());

                for (String ticketNumber : approvedTicketList) {
                    // System.out.println("Query : " + ticketNumber);
                    // ticketList.append("'" + ticketList + "',");

                    ps.setString(1, ticketNumber);
                    ps.addBatch();

                    if (++count % batchSize == 0) {
                        ps.executeBatch();
                        System.out.println("Online Ticket Updated COUNT : " + ps.getUpdateCount());
                    }
                }
                ps.executeBatch();
                System.out.println("Online Ticket Updated COUNT : " + ps.getUpdateCount());
                ps.close();
            } catch (SQLException ex) {
                System.out.println("SQLException: " + ex.getMessage());
            } catch (Exception e) {
                System.out.println("Exception in getUpdateCapitalOneTicket : " + e);
            }

            count = 0;

            // StringBuilder offTicketList = new StringBuilder();

            StringBuilder offTicketUpdateQuery = new StringBuilder("UPDATE RESA_TENDER_DETAILS SET TD_SETTLE_STATUS = '1' WHERE TD_PROCESSOR_ID='603' AND TD_SETTLE_STATUS='0' AND TD_OFFLINE_TICKET_NUMBER IN (?) ");
            System.out.println("Query : " + offTicketUpdateQuery.toString());
            System.out.println("Offline Ticket's to Update : " + offLineApprovedTicketList.size());

            try {
                PreparedStatement ps = con.prepareStatement(offTicketUpdateQuery.toString());

                for (String offticketNumber : offLineApprovedTicketList) {
                    // System.out.println("Query : '" + offticketNumber+"'");
                    // offTicketList.append("'" + offticketNumber + "',");
                    ps.setString(1, offticketNumber);
                    ps.addBatch();

                    if (++count % batchSize == 0) {
                        ps.executeBatch();
                        System.out.println("Offline Ticket's to Update COUNT : " + ps.getUpdateCount());
                    }
                }
                ps.executeBatch();
                System.out.println("Offline Ticket's to Update COUNT : " + ps.getUpdateCount());
                ps.close();
            } catch (SQLException ex) {
                System.out.println("SQLException: " + ex.getMessage());
            } catch (Exception e) {
                System.out.println("Exception in getUpdateCapitalOneTicket : " + e);
            }
            // System.out.println("ticketList : " + ticketList);
            // System.out.println("offTicketList : " + offTicketList);

        }

        public static void main(String[] args) {
            // TODO Auto-generated method stub

        }


    }
