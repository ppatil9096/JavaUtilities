import java.util.ArrayList;
import java.util.HashSet;

class MultiKeyPair {
	Object key;
	Object value;

	public MultiKeyPair(Object key, Object value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}
}

class MultiKeyList extends MultiKeyPair {
	ArrayList<MultiKeyPair> list = new ArrayList<MultiKeyPair>();

	public MultiKeyList(Object key) {
		super(key, null);
	}

	@Override
	public boolean equals(Object obj) {
		list.add((MultiKeyPair) obj);
		return false;
	}

	public static void main(String[] args) {
		HashSet<MultiKeyPair> set = new HashSet<MultiKeyPair>();
		set.add(new MultiKeyPair("A", "2"));
		set.add(new MultiKeyPair("A", "2"));
		set.add(new MultiKeyPair("A", "8"));
		set.add(new MultiKeyPair("A", "8"));
		set.add(new MultiKeyPair("A", "8"));
		set.add(new MultiKeyPair("A", "8"));
		set.add(new MultiKeyPair("A", "8"));
		set.add(new MultiKeyPair("A", "8"));

		MultiKeyList o = new MultiKeyList("A");

		set.contains(o);

		for (MultiKeyPair pair : o.list) {
			System.out.println(pair.value);
		}

	}
}