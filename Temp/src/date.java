import java.util.Calendar;

public class date {/*
					 * 
					 * public static void main(String args[]) {
					 * 
					 * String text = "12.34"; // example String double value =
					 * Double.parseDouble(text) * 100; //
					 * //////////////////////////////////////////////////////////
					 * 
					 * System.out.println("value=" + value);
					 * 
					 * DateFormat fromFormat = new SimpleDateFormat("yyyyMMdd");
					 * fromFormat.setLenient(false); DateFormat toFormat = new
					 * SimpleDateFormat("MMddyyyy"); toFormat.setLenient(false); String dateStr =
					 * "20160515"; Date date; try { date = fromFormat.parse(dateStr);
					 * System.out.println(toFormat.format(date)); } catch (ParseException e) { //
					 * TODO Auto-generated catch block e.printStackTrace(); }
					 * //////////////////////////////////////////////////// String myDate =
					 * "20121206";
					 * 
					 * Calendar cal = Calendar.getInstance(); SimpleDateFormat sdf = new
					 * SimpleDateFormat("yyyyMMdd"); // format try { cal.setTime(sdf.parse(myDate));
					 * } catch (ParseException e) { e.printStackTrace(); }
					 * //////////////////////////////////////////////////////////////// String
					 * currentDate = "20160610"; Date dNow = new Date(); SimpleDateFormat ft = new
					 * SimpleDateFormat("MMddyyyyhhmmss"); currentDate = ft.format(dNow);
					 * System.out.println(currentDate); // System.out.println(ft.format(dNow)); //
					 * SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
					 * //////////////////////////////////////////////////////////////////////////
					 * String a = "9999999999999";
					 * 
					 * try { long b = Long.parseLong(a);
					 * 
					 * System.out.println(b); } catch (NumberFormatException e) {
					 * System.out.println(e); } String SaleCheck = "160256120"; String seqNo =
					 * "00001"; char seqno = '\0'; int seq = 0; String hdramount = "595.00"; String
					 * tnderAmount = "590.00"; int hdraAount = 1100; int tenderAmount = 100;
					 * 
					 * String salecheck = ""; seq = Integer.parseInt(seqNo.replaceFirst("^0+(?!$)",
					 * "")); if (seq != 71) { if (seq == 1) seq = seq + 64; seqno = (char) seq; }
					 * 
					 * if (SaleCheck.length() < 12) { salecheck = String.format("%1$-" + 12 + "s",
					 * SaleCheck); } if (!hdramount.equals(tnderAmount)) { salecheck = "S" +
					 * salecheck + seqno; System.out.println(salecheck); } else if (hdraAount ==
					 * tenderAmount) { salecheck = "P" + salecheck + seqno;
					 * System.out.println(salecheck); } else System.out.println("0000000");
					 * ////////////////////////////////////////////////////////// String s = "fred";
					 * // use this if you want to test the exception below // String s = "100";
					 * 
					 * try { // the String to int conversion happens here int i =
					 * Integer.parseInt(s.trim());
					 * 
					 * // print out the value after the conversion System.out.println("int i = " +
					 * i); } catch (NumberFormatException nfe) {
					 * System.out.println("NumberFormatException: " + nfe.getMessage()); }
					 * 
					 * String d = "12354891489.99"; String c = ""; long b = 132135465319999456l;
					 * 
					 * c = String.valueOf(b); System.out.println(c);
					 * 
					 * }
					 */

	public static void main(String[] args) {

		final String tSaleCode = "SALE";
		final String tReturnCode = "RETURN";
		int count = 1;
		int count1 = 1;

		do {
			final String transationId = getTransactionId(tSaleCode, count);
			System.out.println("TRANSATIONID ::" + transationId);
			count++;
			count1++;
			if (count == 99) {
				count = 1;
			}
		} while (count1 <= 500);

		System.out.println("COUNT :" + count);

		if (count1 == 501) {
			do {
				final String transationId = getTransactionId(tReturnCode, count);
				System.out.println("TRANSATIONID ::" + transationId);
				count++;
				count1++;
				if (count == 99) {
					count = 1;
				}
				if (count1 == 777) {
					System.out.println("count1 :" + count1);
					System.exit(0);
				}
			} while (count1 <= 1000);
		}

	}

	private static String getTransactionId(String Code, int count) {

		Calendar calendar = Calendar.getInstance();
		String transactionId = "";
		String serverId = "17";

		int year = calendar.get(Calendar.YEAR);
		int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		int monthOfYear = calendar.get(Calendar.MONTH);
		int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
		int minuteOfDay = calendar.get(Calendar.MINUTE);
		int secondOfDay = calendar.get(Calendar.SECOND);
		int millisecondOfDay = calendar.get(Calendar.MILLISECOND);

		/*
		 * System.out.println("Year ::" + year); System.out.println("Day of year ::" +
		 * dayOfYear); System.out.println("Day of month ::" + dayOfMonth);
		 * System.out.println("Month of year ::" + monthOfYear);
		 * 
		 * System.out.println("Hour of year ::" + hourOfDay);
		 * System.out.println("Minute of month ::" + minuteOfDay);
		 * System.out.println("Second of year ::" + secondOfDay);
		 * System.out.println("MilliSecond of year ::" + millisecondOfDay);
		 */

		if (Code.equals("SALE")) {
			transactionId = "1" + serverId + Integer.toString(year).substring(2) + Integer.toString(dayOfYear)
					+ Integer.toString(
							(((((hourOfDay * 60) + minuteOfDay) * 60) + secondOfDay) * 1000) + millisecondOfDay)
					+ String.format("%02d", count);
		} else if (Code.equals("RETURN")) {
			transactionId = "2" + serverId + Integer.toString(year).substring(2) + Integer.toString(dayOfYear)
					+ Integer.toString(
							(((((hourOfDay * 60) + minuteOfDay) * 60) + secondOfDay) * 1000) + millisecondOfDay)
					+ String.format("%02d", count);
		}

		return transactionId + "";
	}

}