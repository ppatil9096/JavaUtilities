import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jpos.iso.ISOUtil;
import org.json.JSONObject;

public class PpayRESTClient {
    private static final Logger LOG = LogManager.getLogger(PpayRESTClient.class);
    private static final String HOST="127.0"+".0.1";
    private static final String URL = "http://"+HOST+":8080/ppay/api/ppayServices/transactionService";

    private PpayRESTClient() {
    }

    public static void main(String[] args) {
        String request = "";
        HttpURLConnection connection = null;
        try {
            request = FileUtils.readFileToString(new File("/home/ppatil/workspace/prodSettleWorkspace/auruspay/Documents/Sample_Request/JSONFile.txt"));
            JSONObject jsonObject = new JSONObject(request);
            URL url = new URL(URL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            String payload = "STX" + ISOUtil.byte2hex(jsonObject.get("payload").toString().getBytes()) + "ETX";
            jsonObject.put("payload", payload);
            LOG.info("Request: " + jsonObject);
            OutputStream out = connection.getOutputStream();
            out.write(jsonObject.toString().getBytes());
            out.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String res = new String();
            while ((res = in.readLine()) != null) {
                stringBuilder.append(res);
            }
            LOG.info("Response: " + stringBuilder);

        } catch (Exception e) {
            LOG.error("Exception: ", e);
        } finally {
            if (null != connection) {
                connection.disconnect();
            }
        }
    }
}
